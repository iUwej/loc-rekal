﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Total_Rekal.Model;

namespace Total_Rekal.ViewModel
{
    public class TodoViewModel:INotifyPropertyChanged
    {
        private TodoDataContext toDoDb;
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<TodoItem> allTodoItems;
        private ObservableCollection<TodoItem> pendingTodoItems;
        private ObservableCollection<TodoItem> expiredTodoItems;

        public ObservableCollection<TodoItem> AllTodoItems
        {
            get { return allTodoItems; }
            set
            {
                allTodoItems = value;
                NotifyPropertyChanged("AllTodoItems");
            }
        }

        public ObservableCollection<TodoItem> PendingTodoItems
        {
            get { return pendingTodoItems; }
            set
            {
                pendingTodoItems = value;
                NotifyPropertyChanged("PendingTodoItems");
            }
        }

        public ObservableCollection<TodoItem> ExpiredTodoItems
        {
            get { return expiredTodoItems; }
            set
            {
                expiredTodoItems = value;
                NotifyPropertyChanged("ExpiredTodoItems");
            }
        }


        public TodoViewModel()
        {
            toDoDb = new TodoDataContext(TodoDataContext.DBConnectionString);
        }

        public void LoadDbData()
        {
            var items = toDoDb.TodoItems;
            AllTodoItems = new ObservableCollection<TodoItem>(items);
            //fetch the items by their categories
            DateTime today=DateTime.Now;
            var pendingItems = toDoDb.TodoItems.Where(x => x.IsDone == false && x.Expires >= today);
            PendingTodoItems = new ObservableCollection<TodoItem>(pendingItems);

            var expiredItems = toDoDb.TodoItems.Where(x => x.IsDone == true || x.Expires < today);
            ExpiredTodoItems = new ObservableCollection<TodoItem>(expiredItems);
        }

        public void AddTodoItem(TodoItem item)
        {
            toDoDb.TodoItems.InsertOnSubmit(item);
            toDoDb.SubmitChanges();
            AllTodoItems.Add(item);
            PendingTodoItems.Add(item);
        }

        public void DeleteTodo(TodoItem item)
        {
            toDoDb.TodoItems.DeleteOnSubmit(item);
            toDoDb.SubmitChanges();
            AllTodoItems.Remove(item);
            DateTime today = DateTime.Now;
            if (item.IsDone || item.Expires < today)
            {
                ExpiredTodoItems.Remove(item);
            }
            else
            {
                PendingTodoItems.Remove(item);
            }

        }

        public void saveChangesToDb()
        {
            toDoDb.SubmitChanges();
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
