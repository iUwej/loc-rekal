﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;

namespace Total_Rekal.Model
{
    public class TodoDataContext:DataContext
    {
        public static string DBConnectionString = "Data source=isostore:/Todo.sdf";
        public TodoDataContext(string connString):base(connString)
        {
            
        }

        public Table<TodoItem> TodoItems;
        
    }

    [Table]
    public class TodoItem
    {
        [Column(IsDbGenerated=true,IsPrimaryKey=true)]
        public int Id { get; set; }
        [Column()]
        public string Note { get; set; }
         [Column()]
        public bool IsDone { get; set; }
         [Column()]
        public DateTime Expires { get; set; }
         [Column()]
        public double Longitude { get; set; }
         [Column()]
        public double Latitude { get; set; }
        [Column()]
        public string Venue { get; set; }
    }
}
