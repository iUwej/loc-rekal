﻿#define DEBUG_AGENT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Scheduler;

namespace Total_Rekal
{
    public partial class MainPage : PhoneApplicationPage
    {
        PeriodicTask periodicTask;
        ResourceIntensiveTask resourceIntensiveTask;

        string periodicTaskName = "PeriodicAgent";
        string resourceIntensiveTaskName = "ResourceIntensiveAgent";
        public bool agentsAreEnabled = true;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            StartPeriodicAgent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //StartPeriodicAgent();
        }

        private void StartPeriodicAgent()
        {
            agentsAreEnabled = true;
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // if the the task already exists and background agents are enabled for the application
            // you must remove the task and add it again to update the 
            //schedule
            if (periodicTask != null)
            {
                RemoveAgent(periodicTaskName);
            }

            periodicTask = new PeriodicTask(periodicTaskName);
            periodicTask.Description = "Periodically checks your position and get your saved appointments for  the location";
            try
            {
                ScheduledActionService.Add(periodicTask);
                #if(DEBUG_AGENT)
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(60));
                #endif
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.Contains("BNS Error: This action is disabled"))
                {
                    MessageBox.Show("Background agents for this applications have been disabled");
                    agentsAreEnabled = false;
                }
            }
            catch (SchedulerServiceException ex)
            {
            }
        }

        private void RemoveAgent(string name)
        {
            try
            {
                ScheduledActionService.Remove(name);
            }
            catch (Exception)
            {
            }
        }


    }
}